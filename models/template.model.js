const mongoose = require('mongoose')
const schema = mongoose.Schema

const template_schema = new schema({
    ownerid: {
        type: String, required: true, uinque: true
    },

    template_role: {
        type: String, required: true,
        trim: true,
        minlength: 1
    },
    forminputs: {
        type: Array, required: true,
        trim: true,
        minlength: 1
    },
    // created_at: { type: Date, default: Date.now() },
    // updated_at: { type: Date, default: Date.now() },
});


const template = mongoose.model('templates', template_schema)
module.exports = template;
