const mongoose = require('mongoose')
const schema = mongoose.Schema

const user_schema = new schema({
    username: {
        type: String, required: true,
        trim: true,
        minlength: 1
    },
    email: {
        type: String, required: true, unique: true,
        trim: true,
        minlength: 1
    },
    password: {
        type: String, required: true,
        trim: true,
        minlength: 1
    },
    role: {
        type: String, required: true,
        trim: true,
        minlength: 1
    },
    phone_number: {
        type: String, required: true, unique: true,
        trim: true,
        minlength: 1
    },
    isVerified: { type: Boolean, default: false },
    reset_password_token: String,
    reset_password_expire: Date,
    verification_token: String,
    verification_expire: Date,
    // created_at: { type: Date, default: Date.now() },
    // updated_at: { type: Date, default: Date.now() },

});


const usermodel = mongoose.model('Users', user_schema)
module.exports = usermodel;
