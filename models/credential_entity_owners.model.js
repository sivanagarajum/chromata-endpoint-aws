const mongoose = require('mongoose')
const Schema = mongoose.Schema

const credential_entitySchema = new Schema({
    ownerId: {
        type: String, required: true, uinque: true
    },

    organization: {
        type: String, required: true,
        trim: true
    },

    usage_plan: { type: String, required: true, trim: true },

    api_key: {
        type: String, required: true, unique: true,
        trim: true
    },
    api_keyID: {
        type: String, required: true, unique: true,
        trim: true
    },
    // created_at: { type: Date, default: Date.now() },
    // updated_at: { type: Date, default: Date.now() },

});
const credential_entity = mongoose.model('Entity_Owners', credential_entitySchema)
module.exports = credential_entity;

