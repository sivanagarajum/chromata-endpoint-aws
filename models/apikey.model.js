const mongoose = require('mongoose')
const schema = mongoose.Schema

const Api_schema = new schema({

    api_key: {
        type: JSON, required: true, unique: true,
        trim: true,
        minlength: 1
    },
    api_keyID: {
        type: String, required: true, unique: true,
        trim: true,
        minlength: 1
    },
    // created_at: { type: Date, default: Date.now() },
    // updated_at: { type: Date, default: Date.now() },
});


const ak = mongoose.model('APIkeys', Api_schema)
module.exports = ak;
