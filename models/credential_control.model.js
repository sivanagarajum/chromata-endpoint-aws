const mongoose = require('mongoose');
const schema = mongoose.Schema;

const credentialSchema = new schema({
  entity_owner_id: {
    type: String, required: true,
    trim: true,
    minlength: 1
  },
  recipient_email: {
    type: String, required: true,
    trim: true,
    minlength: 1
  },
  credential_name: {
    type: String, required: true,
    trim: true,
    minlength: 1
  },
  credential_fields: { type: JSON, required: true, trim: true },
  credential_hash: {
    type: String, required: true,
    trim: true
  },
  createdAt: { type: Date, default: Date.now },
  monthyear: {
    type: String
  },
  datemonthyear: {
    type: String
  },
  // created_at: { type: Date, default: Date.now() },
  // updated_at: { type: Date, default: Date.now() },
});

const Credential = mongoose.model('Credentials', credentialSchema);

module.exports = Credential;