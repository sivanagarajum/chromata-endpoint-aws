const mongoose = require('mongoose');
const  env= require('./env')
mongoose.set('useCreateIndex', true)
const connectionString = require('./config')[env].uri;
const connector = mongoose.connect(connectionString, {useNewUrlParser: true,useUnifiedTopology: true})


module.exports = {
    init:()=>{
        
        connector.then(
            () => {
              console.log("Database connection established!");
             
            }            
          ).catch( err => {
            console.log("Error connecting Database  due to: ", err);
          });
        
         
    }
}