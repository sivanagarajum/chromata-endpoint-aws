const router = require('express').Router();
const crypto = require('crypto');
const Credentials = require('../models/credential_control.model');
const count = require('../models/counts.model');


const env = require('./env')
const infura_api_key = require('./config')[env].INFURA_API_KEY;
const privatekey = require('./config')[env].privatekey;
const public_key = require('./config')[env].Public_key_Account1;
const contract_address = require('./config')[env].contract_address;
const contract_abi = require('./config')[env].contract_abi;
const infura_network_url = require('./config')[env].INFURA_NETWORK_URL;


// const bcrypt = require('bcrypt')
var Web3 = require('web3');
const Tx = require('ethereumjs-tx');
const credential_entity = require('../models/credential_entity_owners.model')

const account1 = public_key // Your account address 1
//const account2 = '' // Your account address 2


const web3 = new Web3(new Web3.providers.HttpProvider( infura_network_url+infura_api_key));
web3.eth.defaultAccount = account1;

const Credential_abi = contract_abi

// const Credential_owner = "0xb194e75894e6e67d0bd795e28e3a9996002253cb"; //copy and paste the entity
const Credential_contract_address = contract_address; // req.body.comtractAddress

const credential_abi = Credential_abi;
const credential_Instance = new web3.eth.Contract(credential_abi, Credential_contract_address);


router.route('/createcred_byowner').post((req, res) => {
	const entity_owner_id = req.id
	const recipient_email = req.body.recipient_email;
	const credential_name = req.body.credential_name;
	const credential_fields = req.body.credential_fields;

	if (!recipient_email || !credential_fields || !entity_owner_id || !credential_name) {
		return res.status(400).json({ 'error': "Details should not be empty" });
	}

	const hash = crypto.createHash('sha256').update(JSON.stringify(credential_fields)).digest('hex');
	const credential_hash = hash;



	credential_entity.find({ ownerId: entity_owner_id }, async function (err, fields) {
		if (!fields || !fields.length) {
			console.log('Owner id not valid')
			return res.status(401).json({ 'error': 'Owner id not valid' });
		} else {
			try {
				const date = new Date()
				var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "July", "Aug", "Sept", "Oct", "Nov", "Dec"];
				console.log(date)
				let mon = monthNames[date.getMonth()]
				console.log("The current month is " + monthNames[date.getMonth()]);
				console.log("The current year is " + date.getFullYear());
				const monthyear = mon + date.getFullYear();
				const year = date.getFullYear();
				let dmy = date.getDate()
				const datemonthyear = dmy + mon + year

				const apicount = 0;
				let users = await count.find({ ownerID: entity_owner_id })
				console.log("users", users)
				if (users.length == 0) {
					count.create({
						ownerID: entity_owner_id,
						// user: recipient_email,
						apicountperday: { [datemonthyear]: 0 },
						apicountpermonth: { [monthyear]: 0 }
					})
				}
				const newCredential = new Credentials({
					recipient_email,
					credential_name,
					credential_fields,
					credential_hash,
					entity_owner_id,
					monthyear,
					datemonthyear
				});
				console.log("hash", credential_hash)
				let encoded = credential_Instance.methods.store_credential_hash(credential_hash).encodeABI()
				// encoded = contractInstance.methods.myMethod(params).encodeABI()

				var tx = {
					to: Credential_contract_address,
					data: encoded,
					gas: 3000000,
					gasPrice: 20000000000
				}

				try {
					web3.eth.accounts.signTransaction(tx, privatekey).then(signed => {
						let rec = web3.eth.sendSignedTransaction(signed.rawTransaction)
						rec.then(reci=>console.log(reci))
						newCredential.save()
							.then(() => res.status(200).json({ 'success': 'Credentials created Successfully' }))
							.catch(err => res.status(400).json({ 'error': "something went wrong plz try again" }));
					});
				} catch (e) {
					return res.status(400).json({ 'error': "error in Blockchain credentials not created" });
				}
			} catch (e) {
				return res.status(400).json({ 'error': "something went wrong plz try again" });
			}
		}

	});
})


module.exports = router;