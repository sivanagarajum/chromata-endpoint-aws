const AWS = require('aws-sdk')
const config = require('./config');
const credential_entity = require('../models/credential_entity_owners.model')
const usermodel = require('../models/user.model')
AWS.config.update({
  accessKeyId: config.aws.key,
  secretAccessKey: config.aws.secret,
  region: config.aws.ses.region
});

const jwt = require('jsonwebtoken');


function verifyToken(req, res, next) {
  let token = req.headers['authorization']
  let apikey = req.headers['x-api-key']
  if (apikey) {
    verifyAPI(req, res, next)
  } else {
    if (!token)
      return res.status(401).send({ auth: false, message: 'No token provided.' });

    token = req.headers['authorization']
    
    jwt.verify(token, process.env.SECRET_KEY, function (err, decoded) {
      if (err)
        return res.status(401).send({ auth: false, message: 'Failed to authenticate token.' });
      req.email = decoded.email;
      req.id = decoded._id;
      req.role = decoded.role;
      req.username = decoded.username;
      req.key= decoded.key;
      next()
    });
 
  }
}


function verifyAPI(req, res, next) {
  let apikey = req.headers['x-api-key']
  console.log("apikey", apikey)
  if (!apikey)
    return res.status(401).send({ auth: false, message: 'No apikey provided.' });

  credential_entity.find({ api_key: apikey }, function (err, key) {
    if (!key || !key.length) {
      return res.status(401).json({ 'error': 'api key not valid' });
    } else {
      console.log("key", key)
      console.log("key apikey", key[0].api_key)
      console.log("key api key id", key[0].api_keyID)
      var params = {
        apiKey: key[0].api_keyID, /* required */ //apikey id 
        includeValue: true
      };
      var apigateway = new AWS.APIGateway();
      apigateway.getApiKey(params, function (err, data) {
        console.log("data apikey",data)
        if (err) { return res.status(401).json({ 'error': 'api key not found' });}// an error occurred
        else {
          console.log("data", data)
          console.log("data value", data.value)
          if (data.value === key[0].api_key && data.id===key[0].api_keyID) {
            usermodel.find({ _id: key[0].ownerId }, function (err, users) {
              console.log(users)
              console.log("user email", users[0].email)
              req.email = users[0].email
              console.log("email", req.email)
              req.role = users[0].role
              req.id = users[0]._id
              req.username = users[0].username
              next()
            })
          }
        }
      });
    }
  })

}


module.exports = verifyToken;