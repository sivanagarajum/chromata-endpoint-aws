const serverless = require('serverless-http');
const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const verifyToken = require('./routes/verify-token');
const db =  require("./routes/db");
require('dotenv').config();
const app = express();
// const port = process.env.PORT || 5000;
app.use(cors());
app.use(express.json());



db.init();
const credential_control = require('./routes/credential_control.router');

app.use('/credentials',verifyToken, credential_control);

// app.get('/api/info', (req, res) => {
//   res.send({ application: 'sample-app', version: '1' });
// });
// app.post('/api/v1/getback', (req, res) => {
//   res.send({ ...req.body });
// });
app.get('/', function (req, res) {
  res.send('Hello World!')
})


module.exports.handler = serverless(app);